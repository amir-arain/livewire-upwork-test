<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('form_wizard', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->text('address');
            $table->text('city');
            $table->text('country');
            $table->date('date_of_birth');
            $table->boolean('is_married')->default(false);
            $table->date('date_of_marriage')->nullable();
            $table->string('country_of_marriage')->nullable();
            $table->boolean('is_widowed')->nullable()->default(false);
            $table->boolean('is_previously_married')->nullable()->default(false);;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('form_wizard');
    }
};
