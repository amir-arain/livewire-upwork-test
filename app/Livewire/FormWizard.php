<?php

namespace App\Livewire;

use Carbon\Carbon;
use Livewire\Component;
use App\Models\FormWizard as FormWizardModel;

class FormWizard extends Component
{
    public $step = 1;
    public $title = 'Form Wizard';
    public $months = [];
    public $dates = [];
    public $years = [];
    public $output = [];
    public $stepOneFormData = [
        'first_name' => '',
        'last_name' => '',
        'address' => '',
        'city' => '',
        'country' => '',
        'date_of_birth' => [
            'month' => '',
            'day' => '',
            'year' => '',
        ],
    ];
    public $stepTwoFormData = [
        'is_married' => '',
        'date_of_marriage' => [
            'month' => '',
            'day' => '',
            'year' => '',
        ],
        'country_of_marriage' => '',
        'is_widowed' => null,
        'is_previously_married' => null,
    ];

    public function mount()
    {
        $this->initializeMonths();
        $this->initializeYears();
    }

    public function render()
    {
        return view('livewire.form-wizard');
    }

    public function nextStep()
    {
        if ($this->step == 1) {
            $this->validateStepOne();
            $this->step = $this->validateBirthDay() ? 2 : 1;
        }

    }

    public function previousStep()
    {
        $this->step--;
    }

    public function submit()
    {
        $this->validateStepTwo();

        if ($this->step == 2 && $this->stepTwoFormData['is_married'] == 1) {
            $pass = $this->validateMarriageDate(true);
        } else {
            $pass = $this->validateMarriageDate(false);
        }
        if($pass) {
            $this->stepOneFormData['date_of_birth'] = $this->formatDate($this->stepOneFormData['date_of_birth']);
            $this->output = FormWizardModel::create(array_merge($this->stepOneFormData, $this->stepTwoFormData));
        }

    }

    public function updatedStepOneFormDataDateOfBirthMonth()
    {
        $this->updateDaysForSelectedMonth($this->stepOneFormData['date_of_birth']['month']);
    }

    public function updatedStepTwoFormDataDateOfMarriageMonth()
    {
        $this->updateDaysForSelectedMonth($this->stepTwoFormData['date_of_marriage']['month']);
    }

    public function updatedStepTwoFormDataMaritalStatus($value)
    {
        if ($value == 0) {
            $this->resetStepTwoFormData();
        }
    }

    private function validateStepOne()
    {
        $this->validate($this->getStepOneRules(), $this->getStepOneMessages());
    }

    private function validateStepTwo()
    {
        $this->validate($this->getStepTwoRules(), $this->getStepTwoMessages());
    }

    private function validateBirthDay() {
        $selectedBirthDay = $this->formatDate($this->stepOneFormData['date_of_birth']);
        if ($selectedBirthDay->isFuture()) {
            $this->addError('stepOneFormData.date_of_birth.month', 'The selected date of birth is in the future.');
            return false;
        }
        return true;
    }
    private function validateMarriageDate($type)
    {
        if (!$type) {
            $this->stepTwoFormData['date_of_marriage'] = null;
            $this->stepTwoFormData['country_of_marriage'] = null;
            return true;
        } else {
            $selectedDateOfMarriage = $this->formatDate($this->stepTwoFormData['date_of_marriage']);
            if ($selectedDateOfMarriage->isFuture()) {
                $this->addError('stepTwoFormData.date_of_marriage.month', 'The selected date of marriage is in the future.');
                return false;
            }

            $eighteenthBirthday = $this->formatDate($this->stepOneFormData['date_of_birth'])->addYears(18);
            if ($selectedDateOfMarriage->lessThan($eighteenthBirthday)) {
                $this->addError('stepTwoFormData.date_of_marriage.month', 'You are not eligible to apply because your marriage occurred before your 18th birthday.');
                return false;
            }
            $this->stepTwoFormData['date_of_marriage'] = $this->formatDate($this->stepTwoFormData['date_of_marriage']);
            return true;
        }
    }

    private function getStepOneRules()
    {
        return [
            'stepOneFormData.first_name' => 'required|string|min:2|max:255',
            'stepOneFormData.last_name' => 'required|string|min:2|max:255',
            'stepOneFormData.address' => 'required',
            'stepOneFormData.city' => 'required|string|min:2|max:255',
            'stepOneFormData.country' => 'required|string|min:2|max:255',
            'stepOneFormData.date_of_birth.month' => 'required|between:1,12',
            'stepOneFormData.date_of_birth.day' => 'required|between:1,31',
            'stepOneFormData.date_of_birth.year' => 'required'
        ];
    }

    private function getStepOneMessages()
    {
        return [
            'stepOneFormData.first_name.required' => 'The first name field is required.',
            'stepOneFormData.first_name.string' => 'The first name must be a string.',
            'stepOneFormData.first_name.min' => 'The first name must be at least :min characters.',
            'stepOneFormData.first_name.max' => 'The first name may not be greater than :max characters.',

            'stepOneFormData.last_name.required' => 'The last name field is required.',
            'stepOneFormData.last_name.string' => 'The last name must be a string.',
            'stepOneFormData.last_name.min' => 'The last name must be at least :min characters.',
            'stepOneFormData.last_name.max' => 'The last name may not be greater than :max characters.',

            'stepOneFormData.address.required' => 'The address field is required.',

            'stepOneFormData.city.required' => 'The city field is required.',
            'stepOneFormData.city.string' => 'The city must be a string.',
            'stepOneFormData.city.min' => 'The city must be at least :min characters.',
            'stepOneFormData.city.max' => 'The city may not be greater than :max characters.',

            'stepOneFormData.country.required' => 'The country field is required.',
            'stepOneFormData.country.string' => 'The country must be a string.',
            'stepOneFormData.country.min' => 'The country must be at least :min characters.',
            'stepOneFormData.country.max' => 'The country may not be greater than :max characters.',

            'stepOneFormData.date_of_birth.month.required' => 'The month of birth is required.',
            'stepOneFormData.date_of_birth.month.between' => 'The month of birth must be between :min and :max.',

            'stepOneFormData.date_of_birth.day.required' => 'The day of birth is required.',
            'stepOneFormData.date_of_birth.day.between' => 'The day of birth must be between :min and :max.',
            'stepOneFormData.date_of_birth.year.required' => 'The year of birth is required.',
        ];
    }

    private function getStepTwoRules()
    {
        return [
            'stepTwoFormData.is_married' => 'required|in:0,1',
            'stepTwoFormData.date_of_marriage.month' => 'required_if:stepTwoFormData.is_married,1|nullable|between:1,12',
            'stepTwoFormData.date_of_marriage.day' => 'required_if:stepTwoFormData.is_married,1|nullable|between:1,31',
            'stepTwoFormData.date_of_marriage.year' => 'required_if:stepTwoFormData.is_married,1|nullable',
            'stepTwoFormData.country_of_marriage' => 'required_if:stepTwoFormData.is_married,1|nullable',
            'stepTwoFormData.is_widowed' => 'required_if:stepTwoFormData.is_married,0|nullable|in:0,1',
            'stepTwoFormData.is_previously_married' => 'required_if:stepTwoFormData.is_married,0|nullable|in:0,1'
        ];
    }

    private function getStepTwoMessages()
    {
        return [
            'stepTwoFormData.is_married.required' => 'The marital status field is required.',
            'stepTwoFormData.is_married.in' => 'The marital status field must be either 0 or 1.',
            'stepTwoFormData.date_of_marriage.month.required_if' => 'The month of marriage is required when marital status is married.',
            'stepTwoFormData.date_of_marriage.month.between' => 'The month of marriage must be between 1 and 12.',
            'stepTwoFormData.date_of_marriage.day.required_if' => 'The day of marriage is required when marital status is married.',
            'stepTwoFormData.date_of_marriage.day.between' => 'The day of marriage must be between 1 and 31.',
            'stepTwoFormData.date_of_marriage.year.required_if' => 'The year of marriage is required when marital status is married.',
            'stepTwoFormData.country_of_marriage.required_if' => 'The country of marriage is required when marital status is married.',
            'stepTwoFormData.is_widowed.required_if' => 'The widowed field is required when marital status is not married.',
            'stepTwoFormData.is_widowed.in' => 'The widowed field must be either Yes or No.',
            'stepTwoFormData.is_previously_married.required_if' => 'The previously married field is required when marital status is not married.',
            'stepTwoFormData.is_previously_married.in' => 'The previously married field must be either Yes or No.'
        ];
    }

    private function initializeMonths()
    {
        $this->months = [
            1 => "January",
            2 => "February",
            3 => "March",
            4 => "April",
            5 => "May",
            6 => "June",
            7 => "July",
            8 => "August",
            9 => "September",
            10 => "October",
            11 => "November",
            12 => "December"
        ];
    }

    private function initializeYears()
    {
        $currentYear = now()->format('Y');
        $endYear = now()->subYears(100)->format('Y');
        for ($i = $currentYear; $i >= $endYear; $i--) {
            $this->years[] = $i;
        }
    }

    private function updateDaysForSelectedMonth($prop)
    {
        $this->dates = [];
        $date = Carbon::createFromFormat('m', $prop);
        $firstDayOfMonth = Carbon::create($date->year, $date->month, 1);
        for ($i = 0; $i < $firstDayOfMonth->daysInMonth; $i++) {
            $this->dates[] = $firstDayOfMonth->copy()->addDays($i)->format('d');
        }
    }

    private function resetStepTwoFormData()
    {
        $this->stepTwoFormData['date_of_marriage'] = [
            'month' => '',
            'day' => '',
            'year' => '',
        ];
        $this->stepTwoFormData['country_of_marriage'] = '';
        $this->stepTwoFormData['is_widowed'] = null;
        $this->stepTwoFormData['is_previously_married'] = null;
    }

    private function formatDate($date)
    {
        return Carbon::create($date['year'], $date['month'], $date['day']);
    }
}
