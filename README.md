### Step 1
composer install

### Step 2
npm install

### Step 3
cp .env.example .env

### Step 4
set database in .env

### step 5
php artisan key:generate

### step 6
open a terminal and run php artisan serve

### step 7
open an other terminal and run npm run dev