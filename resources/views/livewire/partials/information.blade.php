<div class="ms-4 py-4 font-semibold text-3xl text-gray-700">
    {{ $title }}
</div>
<div class="ms-4 py-1 font-semibold text-lg text-gray-700">
    Step: {{ $step }}
</div>