<div class="container bg-white mx-auto px-4 mt-12 relative mb-12">
    @empty($output)
        <div class="ms-4 py-4 font-semibold text-3xl text-gray-700">
            {{ $title }}
        </div>
        <div class="ms-4 py-1 font-semibold text-lg text-gray-700">
            Step: {{ $step }}
        </div>
        @if ($step == 1)
            <div class="grid grid-cols-3 grid-flow-col gap-4">
                <div class="mx-4 my-8 flex items-start">
                    <label for="first_name" class="w-28 text-stale-500 font-normal text-md flex-shrink-0">First Name:</label>
                    <div class="w-80">
                        <input type="text" id="first_name" wire:model="stepOneFormData.first_name"
                            class="rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-full @error('stepOneFormData.first_name') border-red-500 @enderror">
                        @error('stepOneFormData.first_name')
                            <span class="!block text-red-500">{{ $message }}</span>
                        @enderror

                    </div>
                </div>
                <div class="mx-4 my-8 flex items-start">
                    <label for="last_name" class="w-28 text-stale-500 font-normal text-md flex-shrink-0">Last Name:</label>
                    <div class="w-80">
                        <input id="last_name" type="text" wire:model="stepOneFormData.last_name"
                            class="rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-full @error('stepOneFormData.first_name') border-red-500 @enderror">
                        @error('stepOneFormData.last_name')
                            <span class="!block text-red-500">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-3 grid-flow-col gap-4">
                <div class="col-span-2 mx-4 my-8 flex items-start">
                    <label for="address" class="w-28 text-stale-500 font-normal text-md flex-shrink-0">Address:</label>
                    <div class="w-full">
                        <textarea id="address" wire:model="stepOneFormData.address"
                            class="rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-full @error('stepOneFormData.address') border-red-500 @enderror"></textarea>
                        @error('stepOneFormData.address')
                            <span class="!block text-red-500">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-3 grid-flow-col gap-4">
                <div class="mx-4 my-8 flex items-start">
                    <label for="city" class="w-28 text-stale-500 font-normal text-md flex-shrink-0">City:</label>
                    <div class="w-80">
                        <input id="city" type="text" wire:model="stepOneFormData.city"
                            class="rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-full @error('stepOneFormData.city') border-red-500 @enderror">
                        @error('stepOneFormData.city')
                            <span class="!block text-red-500">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="mx-4 my-8 flex items-start">
                    <label for="country" class="w-28 text-stale-500 font-normal text-md flex-shrink-0">Country:</label>
                    <div class="w-80">
                        <input id="country" type="text" wire:model="stepOneFormData.country"
                            class="rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-full @error('stepOneFormData.country') border-red-500 @enderror">
                        @error('stepOneFormData.country')
                            <span class="!block text-red-500">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-1 grid-flow-col gap-4">
                <div class="mx-4 my-8 flex items-start">
                    <label class="w-40 me-20 text-stale-500 font-normal text-md flex-shrink-0">Date of Birth:</label>
                    <div class="w-40 me-8">
                        <label for="dob_month" class="block text-stale-500 font-normal text-md flex-shrink-0">Month:</label>
                        <div class="w-40">
                            <select id="dob_month"
                                class="block rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-full @error('stepOneFormData.date_of_birth.month') border-red-500 @enderror"
                                wire:model.change="stepOneFormData.date_of_birth.month">
                                <option value="" wire:key="">---</option>
                                @foreach ($months as $key => $month)
                                    <option value="{{ $key }}" wire:key="{{ $key }}">
                                        {{ $month }}
                                    </option>
                                @endforeach
                            </select>
                            @error('stepOneFormData.date_of_birth.month')
                                <span class="!block text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="w-40 me-8">
                        <label for="dob_day" class="block text-stale-500 font-normal text-md flex-shrink-0">Day:</label>
                        <div class="w-40">
                            <select id="dob_day"
                                class="block rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-full @error('stepOneFormData.date_of_birth.day') border-red-500 @enderror"
                                wire:model.change="stepOneFormData.date_of_birth.day">
                                <option value="" wire:key="">---</option>
                                @foreach ($dates as $key => $date)
                                    <option value="{{ $date }}" wire:key="{{ $key }}">
                                        {{ $date }}
                                    </option>
                                @endforeach
                            </select>
                            @error('stepOneFormData.date_of_birth.day')
                                <span class="!block text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="w-40">
                        <label for="dob_year" class="block text-stale-500 font-normal text-md flex-shrink-0">Year:</label>
                        <div class=" w-40">
                            <select id="dob_day"
                                class="block rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-40 @error('stepOneFormData.date_of_birth.year') border-red-500 @enderror"
                                wire:model="stepOneFormData.date_of_birth.year">
                                <option value="" wire:key="">---</option>
                                @foreach ($years as $key => $year)
                                    <option value="{{ $year }}" wire:key="{{ $key }}">
                                        {{ $year }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @error('stepOneFormData.date_of_birth.year')
                            <span class="!block text-red-500">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>

            <button wire:click="nextStep"
                class="mb-6 ms-4 px-6 py-2 border-3 border-green-800 bg-green-600 text-white rounded-md">
                Next
            </button>
        @else
            <div class="grid grid-cols-3 grid-flow-col gap-4">
                <div class="col-span-2 mx-4 my-4 flex items-start">
                    <label class="w-40 me-20 text-stale-500 font-normal text-md flex-shrink-0">Are You Married?</label>
                    <div class="w-80">
                        <div class="flex items-center justify-start">
                            <label for="are_you_married_yes">Yes</label>
                            <input id="are_you_married_yes" value="1" type="radio"
                                wire:model.live="stepTwoFormData.is_married" class="w-4 ms-2 px-2 py-2">
                            <label for="are_you_married_no" class="ms-8">No</label>
                            <input id="are_you_married_no" value="0" type="radio"
                                wire:model.live="stepTwoFormData.is_married" class="w-4 ms-2 px-2 py-2">
                        </div>
                        @error('stepTwoFormData.is_married')
                            <span class="!block text-red-500">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            @if ($stepTwoFormData['is_married'] == 1)
                <div class="grid grid-cols-1 grid-flow-col gap-4">
                    <div class="mx-4 my-4 flex items-start">
                        <label class="w-40 me-20 text-stale-500 font-normal text-md flex-shrink-0">Date of
                            Marriage:</label>
                        <div class="w-40 me-8">
                            <label class="block text-stale-500 font-normal text-md flex-shrink-0">Month:</label>
                            <select
                                class="block rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-40 @error('stepTwoFormData.date_of_marriage.month') border-red-500 @enderror"
                                wire:model.change="stepTwoFormData.date_of_marriage.month">
                                <option value="" wire:key="">---</option>
                                @foreach ($months as $key => $month)
                                    <option value="{{ $key }}" wire:key="{{ $key }}">
                                        {{ $month }}
                                    </option>
                                @endforeach
                            </select>
                            @error('stepTwoFormData.date_of_marriage.month')
                                <span class="!block text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="w-40 me-8">
                            <label class="block text-stale-500 font-normal text-md flex-shrink-0">Day:</label>
                            <select
                                class="block rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-40 @error('stepTwoFormData.date_of_marriage.day') border-red-500 @enderror"
                                wire:model.change="stepTwoFormData.date_of_marriage.day">
                                <option value="" wire:key="">---</option>
                                @foreach ($dates as $key => $date)
                                    <option value="{{ $date }}" wire:key="{{ $key }}">
                                        {{ $date }}
                                    </option>
                                @endforeach
                            </select>
                            @error('stepTwoFormData.date_of_marriage.day')
                                <span class="!block text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="w-40">
                            <label class="block text-stale-500 font-normal text-md flex-shrink-0">Year:</label>
                            <select
                                class="block rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-40 @error('stepTwoFormData.date_of_marriage.year') border-red-500 @enderror"
                                wire:model="stepTwoFormData.date_of_marriage.year">
                                <option value="" wire:key="">---</option>
                                @foreach ($years as $key => $year)
                                    <option value="{{ $year }}" wire:key="{{ $key }}">
                                        {{ $year }}
                                    </option>
                                @endforeach
                            </select>
                            @error('stepTwoFormData.date_of_marriage.year')
                                <span class="!block text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-3 grid-flow-col gap-4">
                    <div class="col-span-2 mx-4 my-4 flex items-start">
                        <label for="country_of_marriage"
                            class="w-40 me-20 text-stale-500 font-normal text-md flex-shrink-0">Country
                            of
                            Marriage:</label>
                        <div class="w-80">
                            <input id="country_of_marriage" type="text"
                                wire:model="stepTwoFormData.country_of_marriage"
                                class="rounded-lg bg-white border-2 border-gray-200 px-2 py-2 w-full">
                            @error('stepTwoFormData.country_of_marriage')
                                <span class="!block text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            @endif
            @if ($stepTwoFormData['is_married'] == 0)
                <div class="grid grid-cols-3 grid-flow-col gap-4">
                    <div class="col-span-2 mx-4 my-4 flex items-start">
                        <label class="w-40 me-20 text-stale-500 font-normal text-md flex-shrink-0">Are You Widowed?</label>
                        <div class="w-80">
                            <div class="flex items-center justify-start">
                                <label for="widowed-yes" class="">Yes</label>
                                <input id="widowed-yes" value="1" type="radio"
                                    wire:model="stepTwoFormData.is_widowed" class="w-8 ms-4 mx-2 px-2 py-2">
                                <label for="widowed-no" class="">No</label>
                                <input id="widowed-no" value="0" type="radio"
                                    wire:model="stepTwoFormData.is_widowed" class="w-8">
                            </div>
                            @error('stepTwoFormData.is_widowed')
                                <span class="!block text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-3 grid-flow-col gap-4">
                    <div class="col-span-2 mx-4 my-4 flex items-start">
                        <label class="w-40 me-20 text-stale-500 font-normal text-md flex-shrink-0">Previously Married?
                            <span class="text-xs text-gray-500">Have you ever been married in the past?</span>
                        </label>
                        <div class="w-80">
                            <div class="flex items-center justify-start">
                                <label for="previously_married-yes" class="">Yes</label>
                                <input id="previously_married-yes" value="1" type="radio"
                                    wire:model="stepTwoFormData.is_previously_married" class="w-8 ms-4 mx-2 px-2 py-2">
                                <label for="previously_married-no" class="">No</label>
                                <input id="previously_married-no" value="0" type="radio"
                                    wire:model="stepTwoFormData.is_previously_married" class="w-8">
                            </div>
                            @error('stepTwoFormData.is_previously_married')
                                <span class="!block text-red-500">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            @endif
            <button wire:click="previousStep"
                class="mt-8 mb-6 ms-4 px-6 py-2 border-3 border-amber-800 bg-amber-500 text-white rounded-md">
                Previous
            </button>
            <button wire:click="submit"
                class="mt-8 mb-6 ms-4 px-6 py-2 border-3 border-blue-800 bg-blue-500 text-white rounded-md">
                Submit
            </button>
        @endif
    @else
        <div class="relative overflow-x-auto py-4">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 shadow-md sm:rounded-lg">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 overflow-x-auto w-full">
                    <tr>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            First Name
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            Last Name
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            Address
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            City
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            Country
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            Date of birth
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            Married?
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            Date of marriage
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            Country of marriage
                        </th>
                        <th scope="col" class="whitespace-nowrap px-6 py-3">
                            Widowed?
                        </th>
                        <th scope="col" class=" whitespace-nowrap px-6 py-3">
                            Previously married?
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="bg-white border-b ">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap ">
                            {{ $output['first_name'] }}
                        </th>
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                            {{ $output['last_name'] }}
                        </th>
                        <td class="px-6 py-4 whitespace-nowrap overflow-hidden overflow-ellipsis">
                            <div class='has-tooltip'>
                                @if (Illuminate\Support\Str::length($output['address']) > 10)
                                    <span
                                        class='w-auto max-w-80 overflow-hidden whitespace-normal tooltip rounded shadow-lg bg-gray-100 text-blue-600 -mt-12 px-4 py-2'>{{ $output['address'] }}</span>
                                @endif
                                {{ Illuminate\Support\Str::limit($output['address'], 10, '...') }}
                            </div>
                        </td>

                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $output['city'] }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $output['country'] }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ Carbon\Carbon::parse($output['date_of_birth'])->format('m/d/Y') }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $output['is_married'] ? 'Yes' : 'No' }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $output['date_of_marriage'] ? Carbon\Carbon::parse($output['date_of_marriage'])->format('m/d/Y') : '' }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $output['country_of_marriage'] }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $output['is_widowed'] ? 'Yes' : 'No' }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ $output['is_previously_married'] ? 'Yes' : 'No' }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endempty
</div>
